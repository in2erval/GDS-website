from website import create_app
import os

create_app(os.getenv('FLASK_ENV', 'development')).run()