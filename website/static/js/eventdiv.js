class EventDiv {
    // This class is for configuring the div expansion in the events page.

    constructor(divID, size_big, size_small, cutoff) {
        // size_big is the expanded height for large screens, size_small is for small screens.
        // size_small is usually going to be bigger because you need to fit more content in smaller space.
        // Difference between large and small screens is defined by the cutoff.

        this.divID = divID;

        if (window.innerWidth <= cutoff) this.size = size_small;
        else this.size = size_big;

        var context = this;
        window.addEventListener('resize', function(event) {
            if (window.innerWidth <= cutoff) context.size = size_small;
            else context.size = size_big;
            context.state = !context.state;
            context.toggle();
        });

        this.state = false;
    }

    toggle() {
        if (this.state) this.collapse();
        else this.expand();
    }

    expand() {
        document.getElementById(this.divID).style.maxHeight = this.size;
        this.state = true;
    }

    collapse() {
        document.getElementById(this.divID).style.maxHeight = "100px";
        this.state = false;
    }
}