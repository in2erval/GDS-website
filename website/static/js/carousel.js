// Carousel constructor
class Carousel {
  constructor(images, container_ID, transitioner_ID, dots_ID) {
    this.images = images; // List of images to display - can be arbitrarily long as long as the image exists in the path.
    this.timeout = 5000; // Rate at which the picture automatically scrolls, in ms.

    // Script-specific variables - do not change!
    this.current = 0;
    this.max_index = this.images.length - 1;
    this.container = document.getElementById(container_ID);
    this.transitioner = document.getElementById(transitioner_ID);
    this.dots = document.getElementById(dots_ID);
    this.toggle = true;

    // Init.
    this.slide(0);
  }

  slide(index) {
    clearInterval(this.run);
    
    // Transition the background image using two divs.
    if (index <= this.max_index && index >= 0) {
      if (this.toggle) { // Container 1, Transitioner 0
        this.transitioner.style.backgroundImage = `url('${this.images[index]}')`;
        this.transitioner.style.opacity = 1;
        this.container.style.opacity = 0;
        this.toggle = false;
      }
      else { // Container 0, Transitioner 1
        this.container.style.backgroundImage = `url('${this.images[index]}')`;
        this.container.style.opacity = 1;
        this.transitioner.style.opacity = 0;
        this.toggle = true;
      }

      // Create the 'dots' at the bottom.
      this.dots.innerText = "";
      for (var i = 0; i <= this.max_index; i++) {
        if (i === index) {
          if (i === 0) this.dots.innerText += "●";
          else this.dots.innerText += " ●";
        }
        else {
          if (i === 0) this.dots.innerText += "○";
          else this.dots.innerText += " ○";
        }
      }
      this.current = index;

      // Reset interval.
      var context = this;
      this.run = setInterval(function () {
        context.next();
      }, 5000);
    }
  }

  prev() {
    if (this.current === 0) {
      this.slide(this.max_index);
    }
    else {
      this.slide(this.current - 1);
    }
  }

  next() {
    if (this.current === this.max_index) {
      this.slide(0);
    }
    else {
      this.slide(this.current + 1);
    }
  }
}



