from flask import render_template
from werkzeug.exceptions import HTTPException

class InvalidPOST(HTTPException):
    pass

def handle_400():
    return render_template("error.html.j2", code=400)