import os
from . import facebook, errors, resources as res
from flask import Blueprint, render_template, redirect, url_for, current_app, request
from werkzeug.security import check_password_hash


blueprint = Blueprint("web", __name__)

@blueprint.route("/")
@blueprint.route("/index")
def index():
    # Get all images for carousel
    images = []
    for image in os.listdir(os.path.join(current_app.static_folder, 'images/carousel')):
        images.append(image)
    images.sort()

    # Get the next event
    next_event = facebook.next_event()

    return render_template("index.html.j2", images=images, next_event=next_event)

@blueprint.route("/events")
def events():
    return render_template("events.html.j2")

@blueprint.route("/resources")
def resources():
    resources_collection = res.load_resources(current_app.config["RESOURCES_FILE"])
    return render_template("resources.html.j2", resources=resources_collection)

@blueprint.route("/marketing")
def marketing():
    return render_template("marketing.html.j2")

@blueprint.route("/videos")
def videos():
    return render_template("videos.html.j2")

@blueprint.route("/update", methods=["POST"])
def update_fb_token():
    try: 
        if set(request.form.keys()) != set(["password", "token"]): raise Exception()

        with open('data/pwhash.txt', 'r') as hash_file:
            hash = hash_file.read()
        
        if check_password_hash(hash, request.form["password"]):
            new_token = request.form["token"]
            facebook.update_api_token(new_token, current_app.config["FACEBOOK_API_TOKEN_FILE"])
            return redirect(url_for("index"))
        else: raise Exception()
    except:
        raise errors.InvalidPOST()
