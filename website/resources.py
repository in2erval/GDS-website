import re, traceback
from datetime import datetime

class ParseError(Exception):
    def __init__(self, message, linenum):
        self.message = message
        self.linenum = linenum

def load_resources(filename):
    # Load the resources list and return a collections object
    try:
        with open("data/" + filename, "r") as resources_file:
            res_category_regex = re.compile("\\[(.+)\\]$")

            lines = resources_file.read().splitlines()
            resource_collection = []
            
            for index, line in enumerate(lines):
                if line == "": continue

                res_category_match = res_category_regex.match(line)
                if res_category_match:
                    resource_collection.append({ "category": res_category_match.group(1), "subcollections": [] })
                    current_res_category = None
                else:
                    if len(resource_collection) == 0: raise ParseError("a resource category has not been initialised (use '[RESOURCE CATEGORY]')", index + 1)
                    current_res_category = resource_collection[-1]

                    res_args = list(map(lambda l: l.strip(), line.split('|')))
                    if len(res_args) == 1:
                        current_res_category["subcollections"].append({ "type": line, "resources": [] })
                    elif len(res_args) == 3:
                        if len(current_res_category["subcollections"]) == 0: raise ParseError("a resource type has not been initialised (use 'Resource type')", index + 1)
                        current_res_category["subcollections"][-1]["resources"].append({
                                   "name": res_args[0],
                                    "url": res_args[1],
                            "description": res_args[2]
                        })
                    else: raise ParseError("expected 1 or 3 arguments: 'Resource type' or 'Resource name | URL | Description'", index + 1)

            return resource_collection

    except ParseError as err:
        log("Could not load resources - error parsing the resource file: [line {}] {}".format(err.linenum, err.message))
    except:
        log("Could not load resources.")
        traceback.print_exc()
    return None

def log(message):
    print("[resources.py] {}: {}".format(datetime.now(), message))

# Debug
if __name__ == "__main__":
    load_resources("resources.txt")