import json, os, re, requests, traceback
from datetime import date, time, datetime

token_env_variable_name = "FACEBOOK_API_TOKEN"

def next_event():
    # Get the event details from the fetched data file.
    try:
        with open("data/event.json", "r") as event_file:
            event_data = json.load(event_file)
            return event_data
    except:
        log("Could not read next event.")
        return None

def fetch_event():
    # Fetch the next event details from the GDS facebook page.
    # GameDevSoc Group ID: 208371902562261
    access_token = os.getenv(token_env_variable_name, "none")
    try:
        gds_facebook_event_url = "https://graph.facebook.com/v3.1/208371902562261/events?access_token=" + access_token
        contents = requests.get(gds_facebook_event_url).json()
        if "error" in contents:
            log("Error occurred during facebook fetch - is the token expired?")
        elif len(contents["data"]) > 0:
            # Find the closest future event, or if none, the latest event
            sorted_events   = sorted(contents["data"], key=lambda event: parse_datetime(event["start_time"])[3], reverse=True) # Sort by parsed datetime.
            future_events   = list(filter(lambda event: parse_datetime(get_end_time(event))[3] > datetime.now(), sorted_events))
            event_details   = sorted_events[0] if len(future_events) == 0 else future_events[-1]
            event_datetime  = parse_datetime(event_details["start_time"])
            event_location  = find_location(event_details["description"], format_location(event_details["place"]))
            event_data_save = {
                   "title": event_details["name"],
                    "date": event_datetime[0],
                    "time": event_datetime[1],
                   "state": event_datetime[2],
                "location": event_location,
                     "url": "http://www.facebook.com/events/" + event_details["id"]
            }
            log("Successfully fetched data from facebook.")

            if not os.path.exists("data"): os.makedirs("data")
            with open("data/event.json", "w+") as event_file:
                event_file.write(json.dumps(event_data_save))
                log("Overwritten data/event.json.")
        else: raise Exception("Data fetched from facebook was empty.")
    except:
        log("Error occurred during facebook fetch.")
        traceback.print_exc()

def get_end_time(event):
    # Separate function since some events don't have an end_time
    return event["end_time"] if "end_time" in event.keys() else event["start_time"]

def parse_datetime(*data):
    # Expected format example: "2018-09-29T19:00:00+0100"
    datetime_regex   = re.compile("(\\d+)-(\\d+)-(\\d+)T(\\d+):(\\d+)")
    for text in data:
        full_match = datetime_regex.match(text)
        if full_match:
            date_obj = date(int(full_match.group(1)), int(full_match.group(2)), int(full_match.group(3)))
            day      = date_obj.day
            if   day % 10 == 1 and day != 11: day = str(day) + "st"
            elif day % 10 == 2 and day != 12: day = str(day) + "nd"
            elif day % 10 == 3 and day != 13: day = str(day) + "rd"
            else:                             day = str(day) + "th"
            date_string = date_obj.strftime("%A {} %B").format(day)

            datetime_obj = datetime.combine(date_obj, time(int(full_match.group(4)), int(full_match.group(5))))
            time_string  = datetime_obj.strftime("%H:%M")
            state        = ""
            if   datetime_obj < datetime.now(): state = "Previous"
            elif datetime_obj > datetime.now(): state = "Next"
            else:                               state = "Current"
            return (date_string, time_string, state, datetime_obj)
    return ("Not sure when it's happening", "Check facebook for more info", "Next", None)

def format_location(place_json):
    try:
        return "Location: {}".format(place_json["name"])
    except:
        return ""

def find_location(*data):
    # Expected format example: "Location: Teviot Row House - Dining Room"
    location_regex = re.compile("Location:\\s*(.+)")
    for text in data:
        first_match = location_regex.search(text)
        if first_match:
            location_string = first_match.group(1)
            return location_string
    return "Somewhere in Edinburgh"

def load_api_token(filename):
    # Load API Token into environment variables
    try:
        with open("data/" + filename, "r") as token_file:
            token = token_file.read()
            os.environ[token_env_variable_name] = token
    except:
        log("Could not load API token.")

def update_api_token(new_token, filename):
    # Overwrite the environment variable and the token file.
    if not os.path.exists("data"): os.makedirs("data")
    with open("data/" + filename, "w+") as token_file:
        token_file.write(new_token)
        os.environ[token_env_variable_name] = new_token
        log("Updated API token.")

def log(message):
    print("[facebook.py] {}: {}".format(datetime.now(), message))

# Debug
if __name__ == "__main__":
    fetch_event()
    event = next_event()
    if event:
        for key, value in event.items():
            print("{}: {}".format(key, value))