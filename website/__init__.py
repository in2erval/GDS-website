import os, time
from flask     import Flask
from config    import config
from threading import Thread
from .         import app, facebook, errors

def create_app(config_name):
    web = Flask(__name__)
    web.config.from_object(config[config_name])

    facebook.load_api_token(web.config["FACEBOOK_API_TOKEN_FILE"])
    facebook_fetch_thread = Thread(target=facebook_fetch)
    facebook_fetch_thread.start()

    web.register_blueprint(app.blueprint, url_prefix="")
    web.register_error_handler(errors.InvalidPOST, errors.handle_400)
    
    return web

def facebook_fetch():
    while True:
        facebook.fetch_event()
        time.sleep(60)
