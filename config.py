class Config:
    VALUE = "default"
    FACEBOOK_API_TOKEN_FILE = "facebook_token.txt"
    RESOURCES_FILE = "resources.txt"

class DevConfig(Config):
    VALUE = "development"
    DEBUG = True

class ProdConfig(Config):
    VALUE = "production"

config = {
    "development": DevConfig,
     "production": ProdConfig,
        "default": DevConfig
}